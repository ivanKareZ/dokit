# Current State
## Master
[![build status](https://gitlab.com/ivanKareZ/dokit/badges/master/build.svg)](https://gitlab.com/ivanKareZ/dokit/commits/master)
[![coverage report](https://gitlab.com/ivanKareZ/dokit/badges/master/coverage.svg)](https://gitlab.com/ivanKareZ/dokit/commits/master)

## Developer
[![build status](https://gitlab.com/ivanKareZ/dokit/badges/developer/build.svg)](https://gitlab.com/ivanKareZ/dokit/commits/developer)
[![coverage report](https://gitlab.com/ivanKareZ/dokit/badges/developer/coverage.svg)](https://gitlab.com/ivanKareZ/dokit/commits/developer)

---


# Welcome in Dokit

## What is it?

__Dokit__ is a open source documentation-sharing webapp. Released under the MIT license. It is written in java, 
based on the [Spark Framework](http://sparkjava.com/). Storing it's data in a [MySQL](https://www.mysql.com/) database.

With __Dokit__ you can create projects, and add articles under these projects. It allows you to give access to your co-workers/buddies, 
and let them read your ideas and share theirs with you. Your team can edit each others articles, create child-articles and commenting under these super ideas.

It's a really basic webapp, but I think it might be very userful for every startup and organization.

## I get the idea, can i help?

Yes, contributions are welcomed. If these languages/technologies are familiar to u:
* Java
    * Spark
    * Maven
    * JUnit
* Html5
* CSS
    * Bootstrap
* Javascript
    * JQuerry
* English (literally)

For starting, we recommend to read our [Contribution Guide](https://gitlab.com/ivanKareZ/dokit) (It's very empty right now).

## I can't help, but i want to use!

Ohh i see you too. There will be two way to use Dokit. You will be able to host on your own server, or use our webpage. _But there are no released version yet!_

---

_ps: Sorry for wrong grammar, i'm not a native English speaker ;)_