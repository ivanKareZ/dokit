package dokit;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Date;

import org.junit.Test;

import com.ivankarez.dokit.controllers.LoginOutcome;
import com.ivankarez.dokit.db.ActivationOutcome;
import com.ivankarez.dokit.db.MockResultSet;
import com.ivankarez.dokit.db.MockSqlExecutor;
import com.ivankarez.dokit.db.SqlExecutor;
import com.ivankarez.dokit.db.UserDataManager;
import com.ivankarez.dokit.models.User;
import com.ivankarez.dokit.utils.InstanceManager;
import com.ivankarez.dokit.utils.Utils;

public class UserDataManagerTest {

	private final static User USER = new User(-1, "", "", "", "", "", true, new Date());

	@Test
	public void testInsertUser() throws SQLException {
		final MockSqlExecutor executor = new MockSqlExecutor();
		executor.addNonQueryResult(true);
		final MockResultSet result1 = new MockResultSet(1);
		result1.addRow("count", 0);
		executor.addQueryResult(result1);
		final MockResultSet result2 = new MockResultSet(1);
		result2.addRow("count", 0);
		executor.addQueryResult(result2);
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		manager.insertUser(USER);
	}

	@Test
	public void testLogin() throws SQLException {
		final String password = "";
		final MockSqlExecutor executor = new MockSqlExecutor();
		final MockResultSet resultSet = new MockResultSet(2);
		resultSet.addRow("active", 1, "password", Utils.SHA256(password));
		executor.addQueryResult(resultSet);
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		final LoginOutcome result = manager.login("", password);
		assertEquals(LoginOutcome.OK, result);
	}
	
	@Test
	public void testLoginWrongPassword() throws SQLException {
		final String password = "";
		final MockSqlExecutor executor = new MockSqlExecutor();
		final MockResultSet resultSet = new MockResultSet(2);
		resultSet.addRow("active", 1, "password", Utils.SHA256(password));
		executor.addQueryResult(resultSet);
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		final LoginOutcome result = manager.login("", "wrongpassword");
		assertEquals(LoginOutcome.WrongPassword, result);
	}
	
	@Test
	public void testLoginUserNotActive() throws SQLException {
		final String password = "";
		final MockSqlExecutor executor = new MockSqlExecutor();
		final MockResultSet resultSet = new MockResultSet(2);
		resultSet.addRow("active", 0, "password", Utils.SHA256(password));
		executor.addQueryResult(resultSet);
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		final LoginOutcome result = manager.login("", password);
		assertEquals(LoginOutcome.UserNotActivated, result);
	}
	
	@Test
	public void testLoginUserNotExists() throws SQLException {
		final String password = "";
		final MockSqlExecutor executor = new MockSqlExecutor();
		executor.addQueryResult(new MockResultSet(0));
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		final LoginOutcome result = manager.login("", password);
		assertEquals(LoginOutcome.UserNotExist, result);
	}
	
	@Test
	public void testGetUserByUsername() throws SQLException {
		final MockSqlExecutor executor = new MockSqlExecutor();
		final MockResultSet resultSet = new MockResultSet(8);
		resultSet.addRow("id", 0, "username", "", "email", "", "name", "", "password", "", "activationcode", "", "active", 1, "registrationdate", new Date());
		executor.addQueryResult(resultSet);
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		final User result = manager.getUserByUsername("");
		assertNotNull(result);
	}
	
	@Test
	public void testGetUserByUsernameNoUser() throws SQLException {
		final MockSqlExecutor executor = new MockSqlExecutor();
		final MockResultSet resultSet = new MockResultSet(8);
		executor.addQueryResult(resultSet);
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		final User result = manager.getUserByUsername("");
		assertNull(result);
	}

	@Test
	public void testActivateUser() throws Exception {
		final MockSqlExecutor executor = new MockSqlExecutor();
		final MockResultSet resultSet = new MockResultSet(1);
		resultSet.addRow("activationcode", "a");
		executor.addQueryResult(resultSet);
		executor.addNonQueryResult(true);
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		final ActivationOutcome result = manager.activateUser("", "a");
		assertEquals(ActivationOutcome.OK, result);
	}
	
	@Test
	public void testActivateUserWrongActivationCode() throws Exception {
		final MockSqlExecutor executor = new MockSqlExecutor();
		final MockResultSet resultSet = new MockResultSet(1);
		resultSet.addRow("activationcode", "a");
		executor.addQueryResult(resultSet);
		executor.addNonQueryResult(true);
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		final ActivationOutcome result = manager.activateUser("", "x");
		assertEquals(ActivationOutcome.WrongActivationCode, result);
	}
	
	@Test
	public void testActivateUserNonexistingUser() throws Exception {
		final MockSqlExecutor executor = new MockSqlExecutor();
		final MockResultSet resultSet = new MockResultSet(1);
		executor.addQueryResult(resultSet);
		executor.addNonQueryResult(true);
		InstanceManager.putInstance(executor, SqlExecutor.class);

		final UserDataManager manager = new UserDataManager();
		final ActivationOutcome result = manager.activateUser("", "x");
		assertEquals(ActivationOutcome.UserNotExist, result);
	}
	
}
