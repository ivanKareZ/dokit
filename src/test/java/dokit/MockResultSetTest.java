package dokit;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

import com.ivankarez.dokit.db.MockResultSet;

public class MockResultSetTest {

	@SuppressWarnings("resource")
	@Test
	public void test() {
		final MockResultSet resultSet = new MockResultSet(3);
		final Date runDate = new Date();
		resultSet.addRow("name", "karesz", "age", 21, "regdate", new Date());
		
		int steps = 0;
		String name = "";
		int age = -1;
		Date actualDate = new Date();
		while(resultSet.next()){
			steps++;
			name = resultSet.getString("name");
			age = resultSet.getInt("age");
			actualDate = resultSet.getDate("regdate");
		}

		assertEquals(steps, 1);
		assertEquals(name, "karesz");
		assertEquals(age, 21);
		assertEquals(actualDate.compareTo(runDate), 0);
	}
	
	@SuppressWarnings("resource")
	@Test(expected = IllegalArgumentException.class)
	public void testWrongParameterCount() {
		final MockResultSet resultSet = new MockResultSet(3);
		resultSet.addRow("name", "karesz", "age", 21);
	}
	
	@SuppressWarnings("resource")
	@Test
	public void testEmptyResultSet() {
		final MockResultSet resultSet = new MockResultSet(3);
		assertEquals(resultSet.next(), false);
	}
	
}
