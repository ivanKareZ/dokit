package dokit;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.ivankarez.dokit.rendering.HtmlAttribute;
import com.ivankarez.dokit.rendering.HtmlElement;

public class HtmlStringBuilderTest {
	
	@Test
	public void basicTests() {
		final HtmlElement root = new HtmlElement("div");
		assertEquals(root.toString(), "<div></div>");
		
		final HtmlElement root2 = new HtmlElement("div", "row", null);
		assertEquals(root2.toString(), "<div class=\"row\"></div>");
		
		@SuppressWarnings("unused")
		final HtmlElement children = new HtmlElement("p", null, root);
		assertEquals(root.toString(), "<div><p></p></div>");
		
		@SuppressWarnings("unused")
		final HtmlElement children2 = new HtmlElement("p", null, root2);
		assertEquals(root2.toString(), "<div class=\"row\"><p></p></div>");
	}
	
	@Test
	public void setTextTest() {
		final HtmlElement root = new HtmlElement("p");
		root.setText("Hello world!");
		assertEquals(root.toString(), "<p>Hello world!</p>");
	}
	
	@Test
	public void addAttributeTest() {
		final HtmlElement root = new HtmlElement("p");
		root.addAttribute(new HtmlAttribute("id", "paragraph"));
		root.addAttribute(new HtmlAttribute("name", "paragraph"));
		assertEquals(root.toString(), "<p id=\"paragraph\" name=\"paragraph\"></p>");
	}
	
}
