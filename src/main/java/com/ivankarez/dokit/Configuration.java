package com.ivankarez.dokit;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.utils.IOUtils;

public class Configuration {
	
	final static Logger logger = LoggerFactory.getLogger(Configuration.class);
	final static ClassLoader classLoader = Configuration.class.getClassLoader();
	
	String dbUsername;
	String dbPassword;
	String dbAddress;
	String dbSchema;
	int dbPort;
	String secretKey;
	
	public String getDbUsername() {
		return dbUsername;
	}
	
	public String getDbPassword() {
		return dbPassword;
	}
	
	public String getDbAddress() {
		return dbAddress;
	}
	
	public String getDbName() {
		return dbSchema;
	}
	
	public int getDbPort() {
		return dbPort;
	}
	
	public String getSecretKey() {
		return secretKey;
	}
	
	public static Configuration fromFile(String filePath) throws IOException, JSONException {
		final String rawConfig = IOUtils.toString(classLoader.getResourceAsStream(filePath));
		final JSONObject json = new JSONObject(rawConfig);
		final Configuration configuration = new Configuration();
		configuration.secretKey = json.getString("secret_key");
		if(configuration.getSecretKey().equals("")) {
			logger.warn("Secret key is empty. Please provide a secure secret key in your config file!");
		}
		final JSONObject dbJson = json.getJSONObject("database");
		configuration.dbAddress = dbJson.getString("address");
		configuration.dbPort = dbJson.getInt("port");
		configuration.dbUsername = dbJson.getString("username");
		configuration.dbPassword = dbJson.getString("password");
		configuration.dbSchema = dbJson.getString("schema");
		return configuration;
	}
}
