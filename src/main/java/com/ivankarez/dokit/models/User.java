package com.ivankarez.dokit.models;

import java.util.Date;

public class User {

	private int id;
	private String emailAddress;
	private String username;
	private String fullname;
	private String passwordhash;
	private String activationCode;
	private boolean isActive;
	private Date registrationDate;

	public User(int id, String emailAddress, String username, String fullname, String passwordhash, String activationCode,
			boolean isActive, Date registrationDate) {
		super();
		this.id = id;
		this.emailAddress = emailAddress;
		this.username = username;
		this.fullname = fullname;
		this.passwordhash = passwordhash;
		this.activationCode = activationCode;
		this.isActive = isActive;
		this.registrationDate = registrationDate;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getPasswordhash() {
		return passwordhash;
	}

	public void setPasswordhash(String passwordhash) {
		this.passwordhash = passwordhash;
	}

	public String getActivationCode() {
		return activationCode;
	}

	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof User) {
			return ((User)obj).getEmailAddress() == this.getEmailAddress();
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return emailAddress.hashCode();
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id; 
	}
}
