package com.ivankarez.dokit.models;

import java.util.Date;

public class Project {

	private int id;
	private String name;
	private String description;
	private Date createdAt;
	private int creatorId;

	public Project(int id, String name, String description, Date createdAt, int creatorId) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.createdAt = createdAt;
		this.creatorId = creatorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public int getCreatorId() {
		return creatorId;
	}
}
