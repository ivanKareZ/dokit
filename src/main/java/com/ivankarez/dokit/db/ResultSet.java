package com.ivankarez.dokit.db;

import java.sql.SQLException;
import java.util.Date;

public class ResultSet extends ResultSetWrapper {

	private final java.sql.ResultSet delegate;
	
	public ResultSet(java.sql.ResultSet delegate) {
		this.delegate = delegate;
	}
	
	@Override
	public boolean next() {
		try {
			return delegate.next();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int getInt(String name) {
		try {
			return delegate.getInt(name);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String getString(String name) {
		try {
			return delegate.getString(name);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Date getDate(String name) {
		try {
			return delegate.getDate(name);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void close() throws Exception {
		delegate.close();
	}

}
