package com.ivankarez.dokit.db.sql;

import java.util.ArrayList;

public class Select extends SqlExpression {

	private String tableName;
	private ArrayList<String> fields;
	private String condition;
	
	public Select(String tableName) {
		this.tableName = tableName;
		fields = new ArrayList<>();
	}
	
	public void addField(String name) {
		fields.add(name);
	}
	
	public void addCondition(WhereConditionSet conditionset) {
		condition = conditionset.toString();
	}
	
	@Override
	public String build() {
		final StringBuilder sb = new StringBuilder();
		sb.append("SELECT ");
		sb.append(buildFieldString());
		sb.append(" FROM ");
		sb.append(tableName);
		if(condition != null) {
			sb.append(" WHERE ");
			sb.append(condition);
		}
		sb.append(";");
		return sb.toString();
	}
	
	private String buildFieldString() {
		final StringBuilder sb = new StringBuilder();
		if(fields.size() != 0) {
			for (int i = 0; i < fields.size(); i++) {
				sb.append(fields.get(i));
				if(i != fields.size() - 1) {
					sb.append(", ");
				}
			}
		} else {
			sb.append("*");
		}
		return sb.toString();
	}

}
