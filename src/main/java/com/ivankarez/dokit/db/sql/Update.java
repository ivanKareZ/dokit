package com.ivankarez.dokit.db.sql;

import java.util.ArrayList;

public class Update extends SqlExpression {

	protected final String tableName;
	protected final ArrayList<KeyValuePair> kvs;
	protected WhereConditionSet conditions;
	
	public Update(String tableName) {
		this.tableName = tableName;
		kvs = new ArrayList<>();
	}
	
	public void addValue(KeyValuePair kvp) {
		kvs.add(kvp);
	}
	
	public void addConditions(WhereConditionSet conditions) {
		this.conditions = conditions;
	}
 
	@Override
	public String build() {
		final StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		for (KeyValuePair kvp : kvs) {
			sb.append(kvp.getKey());
			sb.append("=");
			sb.append(kvp.getValue());
		}
		sb.append(" WHERE ");
		sb.append(conditions.toString());
		sb.append(";");
		return sb.toString();
	}
	
	
	
}
