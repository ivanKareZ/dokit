package com.ivankarez.dokit.db.sql;

public enum SqlOperator {
	
	EQUALS("="), LESSTHAN("<"), BIGGERTHAN(">"), LESSOREQ("<="), BIGGEROREQ(">="), AND("AND"), OR("OR");

	private final String representation;
	
	public String getRepresentation() {
		return representation;
	}

	SqlOperator(String representation) {
		this.representation = representation;
	}
	
}
