package com.ivankarez.dokit.db;

import java.util.ArrayList;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivankarez.dokit.db.sql.Insert;
import com.ivankarez.dokit.db.sql.KeyValuePair;
import com.ivankarez.dokit.db.sql.Select;
import com.ivankarez.dokit.db.sql.SimpleWhereCondition;
import com.ivankarez.dokit.db.sql.SqlOperator;
import com.ivankarez.dokit.db.sql.WhereConditionSet;
import com.ivankarez.dokit.models.Project;

public class ProjectDataManager extends DataManager {

	final static Logger logger = LoggerFactory.getLogger(ProjectDataManager.class);
	
	public boolean addProject(Project project) {
		final Insert sql = new Insert("project");
		sql.addValue(new KeyValuePair("name", project.getName()));
		sql.addValue(new KeyValuePair("description", project.getDescription()));
		sql.addValue(new KeyValuePair("created", project.getCreatedAt()));
		sql.addValue(new KeyValuePair("creatorid", project.getCreatorId()));
		logger.info("SQL: {}", sql.build());
		final int id = executor.executeAndGetID(sql.build());
		if(id == -1) {
			logger.error("Cannot insert project to the database.");
			return false;
		}
		project = new Project(id, project.getName(), project.getDescription(), project.getCreatedAt(), project.getCreatorId());
		bindProjectToUser(id, project.getCreatorId(), 0);
		return true;
	}
	
	public boolean bindProjectToUser(int projectID, int userId, int level) {
		final Insert sql = new Insert("project_access");
		sql.addValue(new KeyValuePair("userid", userId));
		sql.addValue(new KeyValuePair("projectid", projectID));
		sql.addValue(new KeyValuePair("level", level));
		final int id = executor.executeAndGetID(sql.build());
		if(id == -1) {
			logger.error("Cannot insert project_access to the database. Project id: {} | User id: {} | Level: {}", projectID, userId, level);
			return false;
		}
		return true;
	}
	
	public ArrayList<Project> getProjectsForUser(int userId) {
		final Select sql = new Select("project INNER JOIN project_access ON project.id = project_access.projectid");
		sql.addField("project.id");
		sql.addField("name");
		sql.addField("description");
		sql.addField("created");
		sql.addField("creatorid");
		final WhereConditionSet conditionset = new WhereConditionSet();
		conditionset.addCondition(new SimpleWhereCondition("project_access.userid", userId, SqlOperator.EQUALS));
		sql.addCondition(conditionset);
		
		try(final ResultSetWrapper resultSet = executor.executeQuery(sql.build())) {
			final ArrayList<Project> projects = new ArrayList<>();
			while(resultSet.next()) {
				final int id = resultSet.getInt("id");
				final String name = resultSet.getString("name");
				final String description = resultSet.getString("description");
				final Date created = resultSet.getDate("created");
				final int creatorId = resultSet.getInt("creatorid");
				final Project newProject = new Project(id, name, description, created, creatorId);
				projects.add(newProject);
			}
			return projects;
		} catch (Exception e) {
			logger.error("Cannot list project of user {} cause:", userId, e);
			return new ArrayList<>();
		}
	}

	public Project getProjectById(int projectId) {
		final Select sql = new Select("project");
		final WhereConditionSet conditions = new WhereConditionSet();
		conditions.addCondition(new SimpleWhereCondition("id", projectId, SqlOperator.EQUALS));
		sql.addCondition(conditions);
		try(final ResultSetWrapper resultSet = executor.executeQuery(sql.build())) {
			if(resultSet.next()) {
				final int id = resultSet.getInt("id");
				final String name = resultSet.getString("name");
				final String description = resultSet.getString("description");
				final Date created = resultSet.getDate("created");
				final int creatorId = resultSet.getInt("creatorid");
				final Project newProject = new Project(id, name, description, created, creatorId);
				return newProject;
			} else {
				throw new Exception("Project with id not exists!");
			}
		} catch (Exception e) {
			logger.error("Cannot get project of id {} cause:", projectId, e);
			return null;
		}
	}
	
}
