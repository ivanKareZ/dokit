package com.ivankarez.dokit.db;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class MockResultSet extends ResultSetWrapper {

	final private ArrayList<HashMap<String, Object>> table;
	final int columnCount;
	int selectedRow;
	
	public MockResultSet(int columnCount) {
		table = new ArrayList<>();
		this.columnCount = columnCount;
		selectedRow = -1;
	}
	
	public void addRow(Object... vars) {
		if(vars.length / 2 != columnCount)
			throw new IllegalArgumentException("Row size must be " + columnCount + " but only " + vars.length + " variable given!");
		final HashMap<String, Object> map = new HashMap<>();
		for (int i = 0; i < vars.length; i+=2) {
			map.put((String) vars[i], vars[i+1]);
		}
		table.add(map);
	}
	
	@Override
	public boolean next() {
		selectedRow++;
		return selectedRow < table.size();
	}

	@Override
	public int getInt(String name) {
		return (int) table.get(selectedRow).get(name);
	}

	@Override
	public String getString(String name) {
		return (String) table.get(selectedRow).get(name);
	}

	@Override
	public java.util.Date getDate(String name) {
		return (Date) table.get(selectedRow).get(name);
	}

	@Override
	public void close() throws Exception {}

}
