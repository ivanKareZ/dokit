package com.ivankarez.dokit.db;

import java.util.Date;

public abstract class ResultSetWrapper implements AutoCloseable  {

	public abstract boolean next();
	
	public abstract int getInt(String name);
	
	public abstract String getString(String name);
	
	public abstract Date getDate(String name);
}
