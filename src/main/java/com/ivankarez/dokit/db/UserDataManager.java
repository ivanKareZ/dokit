package com.ivankarez.dokit.db;

import java.sql.SQLException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivankarez.dokit.controllers.LoginOutcome;
import com.ivankarez.dokit.db.sql.Insert;
import com.ivankarez.dokit.db.sql.KeyValuePair;
import com.ivankarez.dokit.db.sql.Select;
import com.ivankarez.dokit.db.sql.SimpleWhereCondition;
import com.ivankarez.dokit.db.sql.SqlOperator;
import com.ivankarez.dokit.db.sql.Update;
import com.ivankarez.dokit.db.sql.WhereConditionSet;
import com.ivankarez.dokit.models.User;
import com.ivankarez.dokit.utils.Utils;

public class UserDataManager extends DataManager {
	
	final static Logger logger = LoggerFactory.getLogger(UserDataManager.class);
	
	public UserDataManager() { }
	
	public User getUserById(int id) {
		return null;
	}
	
	public void insertUser(User user) throws SQLException {
		final Insert insert = new Insert("user");
		insert.addValue(new KeyValuePair("username", user.getUsername()));
		insert.addValue(new KeyValuePair("email", user.getEmailAddress()));
		insert.addValue(new KeyValuePair("name", user.getFullname()));
		insert.addValue(new KeyValuePair("password", user.getPasswordhash()));
		insert.addValue(new KeyValuePair("activationcode", user.getActivationCode()));
		insert.addValue(new KeyValuePair("registrationdate", user.getRegistrationDate()));	
		final int id = executor.executeAndGetID(insert.build());
		user.setId(id);
	}
	
	public boolean isFieldTaken(String field ,String value) throws SQLException {
		final Select select = new Select("user");
		select.addField("count(*) as 'count'");
		final WhereConditionSet conditionset = new WhereConditionSet();
		conditionset.addCondition(new SimpleWhereCondition(field, value, SqlOperator.EQUALS));
		select.addCondition(conditionset);
		final String sql = select.build();
		final ResultSetWrapper result = executor.executeQuery(sql);
		result.next();
		final int count = result.getInt("count");
		try {
			result.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return count != 0;
	}
	
	public ActivationOutcome activateUser(String username, String activationCode) throws Exception {
		final Select select = new Select("user");
		select.addField("activationcode");
		final WhereConditionSet conditionset = new WhereConditionSet();
		conditionset.addCondition(new SimpleWhereCondition("username", username, SqlOperator.EQUALS));
		select.addCondition(conditionset);
		final String sql = select.build();
		try(final ResultSetWrapper result = executor.executeQuery(sql)) {		
			if(result.next()){
				final String dbActivationCode = result.getString("activationcode");
				if(dbActivationCode.equals(activationCode)) {					
					final Update update = new Update("user");
					final WhereConditionSet conditions = new WhereConditionSet();
					conditions.addCondition(new SimpleWhereCondition("username", username, SqlOperator.EQUALS));
					update.addValue(new KeyValuePair("active", 1));
					update.addConditions(conditions);
					executor.executeNonQuery(update.build());
					return ActivationOutcome.OK;
				} else {
					return ActivationOutcome.WrongActivationCode;
				}
			} else {
				return ActivationOutcome.UserNotExist;
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public LoginOutcome login(String username, String password) throws SQLException {
		final Select select = new Select("user");
		final WhereConditionSet set = new WhereConditionSet();
		set.addCondition(new SimpleWhereCondition("username", username, SqlOperator.EQUALS));
		select.addCondition(set);
		select.addField("password");
		select.addField("active");

		final String passwordHash = Utils.SHA256(password);
		final String sql = select.build();
		final ResultSetWrapper result = executor.executeQuery(sql);
		if (result.next()) {
			if(result.getInt("active") != 1){
				return LoginOutcome.UserNotActivated;
			}
			if (!result.getString("password").equals(passwordHash)) {
				return LoginOutcome.WrongPassword;
			}
		} else {
			return LoginOutcome.UserNotExist;
		}
		return LoginOutcome.OK;
	}

	public User getUserByUsername(String pusername) {
		final Select select = new Select("user");
		final WhereConditionSet set = new WhereConditionSet();
		set.addCondition(new SimpleWhereCondition("username", pusername, SqlOperator.EQUALS));
		select.addCondition(set);
		
		final String sql = select.build();
		try(final ResultSetWrapper result = executor.executeQuery(sql)) {		
			if(result.next()){
				final int id = result.getInt("id");
				final String username = result.getString("username");
				final String email = result.getString("email");
				final String name = result.getString("name");
				final String password = result.getString("password");
				final String activationcode = result.getString("activationcode");
				final boolean active = result.getInt("active") == 1;
				final Date regDate = result.getDate("registrationdate");
				return new User(id, email, username, name, password, activationcode, active, regDate);
			}
		} catch (Exception e) {
			logger.error("Cannot get user from database, cause: ", e);
		}
		return null;
	}
}
