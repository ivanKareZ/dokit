package com.ivankarez.dokit.db;

public enum ActivationOutcome {
	OK, WrongActivationCode, UserNotExist
}
