package com.ivankarez.dokit.db;

import java.sql.SQLException;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class MockSqlExecutor implements SqlExecutor {
	
	private Queue<ResultSetWrapper> executeQueryResults;
	private Queue<Boolean> executeNonQueryResults;
	private Queue<Integer> executeAndGetIDresults;
	
	public MockSqlExecutor() {
		executeQueryResults = new LinkedBlockingQueue<>();
		executeNonQueryResults = new LinkedBlockingQueue<>();
		executeAndGetIDresults = new LinkedBlockingQueue<>();
	}

	@Override
	public ResultSetWrapper executeQuery(String query) throws SQLException {
		return executeQueryResults.poll();
	}

	@Override
	public boolean executeNonQuery(String query) throws SQLException {
		return executeNonQueryResults.poll();
	}

	@Override
	public int executeAndGetID(String query) {
		return executeAndGetIDresults.poll();
	}
	
	public void addQueryResult(ResultSetWrapper result) {
		executeQueryResults.add(result);
	}
	
	public void addNonQueryResult(boolean result) {
		executeNonQueryResults.add(result);
	}
	
	public void addQueryAndGetIDresults(int result) {
		executeAndGetIDresults.add(result);
	}
}
