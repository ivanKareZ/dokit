package com.ivankarez.dokit.db;

import java.sql.SQLException;

public interface SqlExecutor {
	
	ResultSetWrapper executeQuery(String query) throws SQLException;
	boolean executeNonQuery(String query) throws SQLException;
	int executeAndGetID(String query);
	
}
