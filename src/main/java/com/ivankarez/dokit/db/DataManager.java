package com.ivankarez.dokit.db;

import com.ivankarez.dokit.utils.InstanceManager;

public abstract class DataManager {
	final protected SqlExecutor executor;
	
	public DataManager() {
		executor = InstanceManager.getInstance(SqlExecutor.class);
	}
}
