package com.ivankarez.dokit.rendering;

import java.util.ArrayList;

public class HtmlLayoutBuilder {
	
	protected String title;
	protected ArrayList<String> scripts;
	protected ArrayList<String> styles;
	protected ArrayList<String> headers;
	protected ArrayList<String> bodyContents;
	
	public HtmlLayoutBuilder() {
		title = "-";
		scripts = new ArrayList<>();
		styles = new ArrayList<>();
		headers = new ArrayList<>();
		bodyContents = new ArrayList<>();
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void addBodyContent(String bodyContent) {
		bodyContents.add(bodyContent);
	}
	
	public void addScript(String script) {
		scripts.add(script);
	}
	
	public void addStyle(String style) {
		styles.add(style);
	}
	
	public void addHeader(String header) {
		headers.add(header);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("<!DOCTYPE html>");
		sb.append("<html>");
		sb.append("<head>");
		sb.append("<meta charset=\"UTF-8\">");
		sb.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
		sb.append("<title>" + title + "</title>");
		for (String style : styles) {
			sb.append("<link rel='stylesheet' href='" + style + "'>");
		}
		for (String headerLine : headers) {
			sb.append(headerLine);
		}
		sb.append("</head>");
		sb.append("<body>");
		for (String bodyLine : bodyContents) {
			sb.append(bodyLine);
		}
		for (String script : scripts) {
			sb.append("<script src='" + script + "'></script>");
		}
		sb.append("</body>");
		sb.append("</html>");
		return sb.toString().replace('\n', ' ');
	}
	
}
