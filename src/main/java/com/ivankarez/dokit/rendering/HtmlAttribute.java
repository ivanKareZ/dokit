package com.ivankarez.dokit.rendering;

public class HtmlAttribute {

	private String name;
	private String value;

	public HtmlAttribute(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append(name);
		sb.append("=\"");
		sb.append(value);
		sb.append("\"");
		return sb.toString();
	}
}
