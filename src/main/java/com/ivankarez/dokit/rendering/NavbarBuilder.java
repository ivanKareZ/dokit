package com.ivankarez.dokit.rendering;

import java.util.ArrayList;

import com.ivankarez.dokit.models.User;

public class NavbarBuilder {
	
	ArrayList<MenuItem> items;
	User user;
	
	public NavbarBuilder(){
		this("");
	}
	
	public NavbarBuilder(String currentLink) {
		items = new ArrayList<>();
		AddMenuItem(new MenuItem(false, "/", "Home"));
		AddMenuItem(new MenuItem(false, "/create", "Add"));
		for (MenuItem menuItem : items) {
			if (menuItem.getLocation().equals(currentLink)) {
				menuItem.isActive = true;
			}
		}
	}
	
	@Override
	public String toString() {
		return build();
	}
	
	@SuppressWarnings("unused")
	public String build() {
		final HtmlElement root = new HtmlElement("nav", "navbar navbar-default", null);
		final HtmlElement container = new HtmlElement("div", "container-fluid", root);
		final HtmlElement header = new HtmlElement("div", "navbar-header", container);
		
		final HtmlElement navbutton = new HtmlElement("button", "navbar-toggle collapsed", header);
		navbutton.addAttribute(new HtmlAttribute("type", "button"));
		navbutton.addAttribute(new HtmlAttribute("data-toggle", "collapse"));
		navbutton.addAttribute(new HtmlAttribute("data-target", "#navbar"));
		navbutton.addAttribute(new HtmlAttribute("aria-expanded", "false"));
		navbutton.addAttribute(new HtmlAttribute("aria-controls", "navbar"));
		final HtmlElement togglespan = new HtmlElement("span", "sr-only", navbutton);
		final HtmlElement bar1 = new HtmlElement("span", "icon-bar", navbutton);
		final HtmlElement bar2 = new HtmlElement("span", "icon-bar", navbutton);
		final HtmlElement bar3 = new HtmlElement("span", "icon-bar", navbutton);
		
		final HtmlElement brand = new HtmlElement("a", "navbar-brand", header);
		brand.setText("Dokit");
		brand.addAttribute(new HtmlAttribute("href", "/"));
		
		final HtmlElement navbarDiv = new HtmlElement("div", "navbar-collapse collapse", container);
		final HtmlElement navbarListLeft = new HtmlElement("ul", "nav navbar-nav", navbarDiv);
		if(user != null) {
			for (MenuItem menuItem : items) {
				final HtmlElement listElement = new HtmlElement("li", null, navbarListLeft);
				if(menuItem.isActive) {
					listElement.addAttribute(new HtmlAttribute("class", "active"));
				}
				final HtmlElement link = new HtmlElement("a", null, listElement);
				link.addAttribute(new HtmlAttribute("href", menuItem.getLocation()));
				link.setText(menuItem.getText());
			}
		}
		final HtmlElement navbarRightDiv = new HtmlElement("div", "nav navbar-nav navbar-right", navbarDiv);
		final HtmlElement navbarListRight = new HtmlElement("ul", "nav navbar-nav", navbarRightDiv);
		if(user != null) {
			final HtmlElement userGui = new HtmlElement("li", null, navbarListRight);
			final HtmlElement userGuiLink = new HtmlElement("a", null, userGui);
			userGuiLink.addAttribute(new HtmlAttribute("href", "#"));
			final HtmlElement bold = new HtmlElement("b", null, userGuiLink);
			bold.setText(user.getFullname());
		}
		
		return root.toString();
	}
	
	public void AddMenuItem(MenuItem item) {
		items.add(item);
	}
	
	protected String getMenuItems() {
		StringBuilder sb = new StringBuilder();
		if(user != null) {
			for (MenuItem menuItem : items) {
				if(menuItem.isActive) {
					sb.append("<li class=\"active\"><a href=\""+menuItem.getLocation()+"\">"+menuItem.getText()+"</a></li>");
				} else {
					sb.append("<li><a href=\""+menuItem.getLocation()+"\">"+menuItem.getText()+"</a></li>");
				}
			}
		}
		return sb.toString();
	}
	
	public void putUser(User user) {
		this.user = user;
	}
	
	public class MenuItem {
		private boolean isActive;
		private String location;
		private String text;

		public MenuItem(boolean isActive, String location, String text) {
			super();
			this.isActive = isActive;
			this.location = location;
			this.text = text;
		}

		public boolean isActive() {
			return isActive;
		}

		public String getLocation() {
			return location;
		}

		public String getText() {
			return text;
		}	
	}
}
