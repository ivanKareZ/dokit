package com.ivankarez.dokit.controllers;

public enum LoginOutcome {
	OK, UserNotExist, WrongPassword, UserNotActivated
}
