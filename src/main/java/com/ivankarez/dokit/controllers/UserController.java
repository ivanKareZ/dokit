package com.ivankarez.dokit.controllers;

import com.ivankarez.dokit.db.UserDataManager;
import com.ivankarez.dokit.models.User;
import com.ivankarez.dokit.utils.Utils;

public class UserController {
	
	final UserDataManager manager;
	
	public UserController() {
		manager = new UserDataManager();
	}
	
	public LoginOutcome login(String username, String password) {
		final User user = manager.getUserByUsername(username);
		if(user == null)
			return LoginOutcome.UserNotExist;
		
		if(!user.isActive())
			return LoginOutcome.UserNotActivated;
		
		final String passwordHash = Utils.SHA256(password);
		if(!user.getPasswordhash().equals(passwordHash))
			return LoginOutcome.WrongPassword;
		return LoginOutcome.OK;
	}
	
	public RegistrationOutcome registration(User user) throws Exception {
		if (manager.isFieldTaken("username", user.getUsername())) {
			return RegistrationOutcome.UsernameIsTaken;
		}
		if (manager.isFieldTaken("email", user.getEmailAddress())) {
			return RegistrationOutcome.EmailIsTaken;
		}
		manager.insertUser(user);
		return RegistrationOutcome.OK;
	}
	
}
