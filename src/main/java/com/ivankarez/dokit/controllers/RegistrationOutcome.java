package com.ivankarez.dokit.controllers;

public enum RegistrationOutcome {
	OK, UsernameIsTaken, EmailIsTaken
}
