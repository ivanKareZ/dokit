package com.ivankarez.dokit.services;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivankarez.dokit.Service;
import com.ivankarez.dokit.db.ActivationOutcome;
import com.ivankarez.dokit.db.UserDataManager;
import com.ivankarez.dokit.rendering.DefaultHtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.HtmlLayoutBuilder;
import com.ivankarez.dokit.rendering.NavbarBuilder;
import com.ivankarez.dokit.rendering.TemplateReader;

import spark.Request;
import spark.Response;

public class ActivateService extends Service {

	private static final Logger logger = LoggerFactory.getLogger(ActivateService.class);
	private final String activateForm;
	private UserDataManager dataManager;
	
	public ActivateService(String bindingPath) {
		super(bindingPath);
		activateForm = new TemplateReader("templates/activate.html").read();
		dataManager = new UserDataManager();
	}

	@Override
	public String post(Request req, Response res) {
		try{
			final JSONObject json = new JSONObject(req.body());
			final String username = json.getString("username");
			final String activationCode = json.getString("activationcode");
			final ActivationOutcome result = dataManager.activateUser(username, activationCode);
			switch (result) {
			case UserNotExist:
				return buildSimpleResult(1, "User does not exist!");
			case WrongActivationCode:
				return buildSimpleResult(1, "Wrong activation code!");
			case OK:
				return buildSimpleResult(0, "OK");
			default:
				throw new IllegalStateException("Result value was something unhandled! Should not reach this line.");
			}
		} catch (Exception e) {
			logger.error("Error while activating user! Cause:", e);
			return buildInternalErrorResponse();
		}
	}

	@Override
	public String get(Request req, Response res) {
		if(getUser() != null) {
			redirect("/");
		}
		final HtmlLayoutBuilder builder = new DefaultHtmlLayoutBuilder();
		builder.setTitle("Dokit - Login");
		builder.addScript("js/activate.js");
		builder.addStyle("style/login.css");
		final NavbarBuilder navbar = new NavbarBuilder();
		navbar.putUser(null);
		builder.addBodyContent(navbar.toString());
		builder.addBodyContent(activateForm);
		return builder.toString();
	}

}
