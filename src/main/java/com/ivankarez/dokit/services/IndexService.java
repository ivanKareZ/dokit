package com.ivankarez.dokit.services;

import java.util.ArrayList;

import com.ivankarez.dokit.Service;
import com.ivankarez.dokit.db.ProjectDataManager;
import com.ivankarez.dokit.models.Project;
import com.ivankarez.dokit.rendering.*;
import com.ivankarez.dokit.utils.Utils;

import spark.Request;
import spark.Response;

public class IndexService extends Service {

	public IndexService(String bindingPath) {
		super(bindingPath);
	}

	@Override
	public String get(Request req, Response res) {
		if(getUser() == null) return redirect("/login");
		
		final ProjectDataManager pdb = new ProjectDataManager();
		final ArrayList<Project> projects = pdb.getProjectsForUser(getUser().getId());
		
		final HtmlLayoutBuilder builder = new DefaultHtmlLayoutBuilder();
		final NavbarBuilder navbarBuilder = new NavbarBuilder("/");
		builder.addScript("js/index.js");
		navbarBuilder.putUser(getUser());
		builder.setTitle("Dokit - Home");
		builder.addBodyContent(navbarBuilder.toString());
		builder.addBodyContent(buildPage(projects));
		return builder.toString();
	}
	
	@SuppressWarnings("unused")
	private String buildPage(ArrayList<Project> projects) {
		final HtmlElement root = new HtmlElement("div", "container", null);
		final HtmlElement row = new HtmlElement("div","row", root);
		final HtmlElement column = new HtmlElement("div", "col-md-12", row);
		final HtmlElement panelRoot = new HtmlElement("div", "panel panel-default", column);
		final HtmlElement panelHeader = new HtmlElement("div", "panel-heading", panelRoot);
		panelHeader.setText("Your projects");
		final HtmlElement panelBody = new HtmlElement("div", "panel-body", panelRoot);
		panelBody.addAttribute(new HtmlAttribute("id", "projectlist"));
		if(projects.size() > 0) {
			final HtmlElement listDiv = new HtmlElement("div", "list-group", panelBody);
			for (Project project : projects) {
				final HtmlElement projectText = new HtmlElement("a","list-group-item", listDiv);
				projectText.addAttribute(new HtmlAttribute("href", "/project/"+project.getId()));
				final HtmlElement title = new HtmlElement("h4", "list-group-item-heading", projectText);
				title.setText(project.getName());
				final HtmlElement description = new HtmlElement("p", "list-group-item-text", projectText);
				description.setText(Utils.trimIfLonger(project.getDescription(), 100));
				description.addAttribute(new HtmlAttribute("style", "color:#A0A0A0;"));
			}
		}
		final HtmlElement addButton = new HtmlElement("button", "btn btn-primary", column);
		addButton.addAttribute(new HtmlAttribute("id", "create-project-button"));
		addButton.addAttribute(new HtmlAttribute("style", "float: right;"));
		final HtmlElement plusGliph = new HtmlElement("span", "glyphicon plus", addButton);
		final HtmlElement text = new HtmlElement("span", null, addButton);
		text.setText("Add project");
		return root.toString();
	}

	@Override
	public String post(Request req, Response res) {
		res.status(404);
		return "";
	}

}
