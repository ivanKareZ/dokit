package com.ivankarez.dokit.services;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ivankarez.dokit.Service;

import spark.Request;
import spark.Response;
import spark.utils.IOUtils;

public class SimpleFileProviderService extends Service {

	final static Logger logger = LoggerFactory.getLogger(SimpleFileProviderService.class);

	public SimpleFileProviderService(String bindingPath) {
		super(bindingPath);
	}

	@Override
	public String get(Request req, Response res) {
		final ClassLoader classLoader = getClass().getClassLoader();
		String fileName = req.uri().substring(1);
		logger.info("File name: "+ fileName);
		String result = "";
		try {
			result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
		} catch (IOException e) {
			logger.error("Error while reading file content: {}\n", fileName, e);
		}
		
		String resourceType = getFileExtension(fileName);
		if(resourceType.equals("css")) {
			res.type("text/css");
		} else if(resourceType.equals("js")) {
			res.type("text/javascript");
		} /*else if(resourceType.equals("woff2") || resourceType.equals("woff") || resourceType.equals("ttf")) {
			res.type("font/opentype");
		}*/
		return result;
	}
	
	private String getFileExtension(String filePath) {
		String extension = "plain";

		int i = filePath.lastIndexOf('.');
		if (i > 0) {
		    extension = filePath.substring(i+1);
		}
		return extension;
	}

	@Override
	public String post(Request req, Response res) {
		res.status(404);
		return "";
	}

}
