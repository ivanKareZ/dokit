$(function() {
	$('#project-form').attr("onSubmit", "return validateLogin()");

});

function validateLogin() {
	var errors = [];
	var name = $("#projectname").val();
	var description = $("#description").val();
	var id = $("#id").val();
	if(name === "") {
		showError("Project name cannot be empty!");
		return false;
	}
	var data = {};
	data.name = name;
	data.description = description;
	data.id = id;
	$.ajax({
	    url: '/project',
	    type: 'POST',
	    data: JSON.stringify(data),
	    contentType: 'application/json; charset=utf-8',
	    dataType: 'json',
	    async: true,
	    success: function(msg) {
	        if(msg.result != 0) {
	        	showError(msg.message);
	        }
	        else {
	        	window.location.replace("/");
	        }
	    },
      	error: function (xhr, ajaxOptions, thrownError) {
      		showError("Unkown error. ("+xhr.status+")");
      	}
	});
	return false;
}

function showError(text)  {
	$("#error-text").html(text);
	$("#error-panel").css('visibility', 'visible');
	$("#error-panel").show("fast", function() {
		
	});
}